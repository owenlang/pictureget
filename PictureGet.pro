QT += core gui
QT += webkit
QT += network

FORMS += \
    form.ui

HEADERS += \
    pictureget.h

SOURCES += \
    pictureget.cpp \
    main.cpp
