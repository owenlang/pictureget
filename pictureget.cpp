/*-----------------------------------------------------------------------------
Description: ͼƬ��ȡ��
Author: WRW	 1229567637@QQ.COM
Date: 2013/11/20
-----------------------------------------------------------------------------*/
#include <QtGui>
#include <QtWebKit/QtWebKit>
#include <QtNetwork>
#include <QDateTime>

#include "pictureget.h"

PictureGet::PictureGet(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    initData();
}

void PictureGet::initData()
{
    nam_ = new QNetworkAccessManager();
    pixmap_ = new QPixmap();
    retry_counter_ = 3;
    progressBar->setValue(0);
    connect(nam_, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
    connect(quitBtn, SIGNAL(clicked()), qApp, SLOT(quit()));
}

void PictureGet::getPic(const QString &url)
{
    nam_->get(QNetworkRequest(QUrl(url)));
}

void PictureGet::on_startBtn_clicked()
{
    qDebug("start...");
    QString url = urlEdit->toPlainText().trimmed();
    bool ok;
    int count = numberEdit->text().toInt(&ok, 10);
    progressBar->setRange(0, count);
    for (int i = 0; i < count; ++i) {
        getPic(url);
    }
    progressBar->reset();
}

void PictureGet::replyFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError) {
        qDebug("reply finished and no error");
        pixmap_->loadFromData(reply->readAll());
        static int i = 0;
        QString filename = "picture/" + QString("%1.jpg").arg(i++);
        if (pixmap_->save(filename)) {
            qDebug() << "picture saved as" << filename;
            progressBar->setValue(i);
        }
    } else {
        qDebug("reply error");
    }
}

PictureGet::~PictureGet()
{
    delete pixmap_;
    delete nam_;
}
