/*-----------------------------------------------------------------------------
Description: ͼƬ��ȡ��
Author: WRW	 1229567637@QQ.COM
Date: 2013/11/20
-----------------------------------------------------------------------------*/
#ifndef PictureGet_H
#define PictureGet_H

#include <QWidget>

#include "ui_form.h"

class QUrl;
class QNetworkAccessManager;
class QNetworkReply;
class QWebView;
class QPixmap;
class QPaintEvent;
class QString;

class PictureGet : public QWidget, public Ui::PictureGet
{
    Q_OBJECT
public:
    explicit PictureGet(QWidget *parent = 0);
    virtual ~PictureGet();
    void getPic(const QString &url);
signals:
    
public slots:
    void replyFinished(QNetworkReply *reply);
    void on_startBtn_clicked();

private:
    void initData();

    QNetworkAccessManager *nam_;
    int retry_counter_;
    QPixmap *pixmap_;
};

#endif // PictureGet_H
