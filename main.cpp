/*-----------------------------------------------------------------------------
Description: 批量下载验证码图片
Author: WRW	 1229567637@QQ.COM
Date: 2013/11/20
-----------------------------------------------------------------------------*/
#include <QApplication>
#include "pictureget.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    PictureGet pic_get;
    pic_get.show();

    return app.exec();

}
