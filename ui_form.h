/********************************************************************************
** Form generated from reading UI file 'form.ui'
**
** Created: Wed Nov 20 20:11:43 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_H
#define UI_FORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PictureGet
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QTextEdit *urlEdit;
    QSpacerItem *verticalSpacer;
    QProgressBar *progressBar;
    QLabel *label_2;
    QLineEdit *numberEdit;
    QSpacerItem *horizontalSpacer;
    QPushButton *startBtn;
    QPushButton *quitBtn;

    void setupUi(QWidget *PictureGet)
    {
        if (PictureGet->objectName().isEmpty())
            PictureGet->setObjectName(QString::fromUtf8("PictureGet"));
        PictureGet->resize(523, 144);
        gridLayout = new QGridLayout(PictureGet);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(PictureGet);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        urlEdit = new QTextEdit(PictureGet);
        urlEdit->setObjectName(QString::fromUtf8("urlEdit"));

        gridLayout->addWidget(urlEdit, 0, 1, 2, 4);

        verticalSpacer = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);

        progressBar = new QProgressBar(PictureGet);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(24);

        gridLayout->addWidget(progressBar, 2, 0, 1, 5);

        label_2 = new QLabel(PictureGet);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 3, 0, 1, 1);

        numberEdit = new QLineEdit(PictureGet);
        numberEdit->setObjectName(QString::fromUtf8("numberEdit"));

        gridLayout->addWidget(numberEdit, 3, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(138, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 3, 2, 1, 1);

        startBtn = new QPushButton(PictureGet);
        startBtn->setObjectName(QString::fromUtf8("startBtn"));

        gridLayout->addWidget(startBtn, 3, 3, 1, 1);

        quitBtn = new QPushButton(PictureGet);
        quitBtn->setObjectName(QString::fromUtf8("quitBtn"));

        gridLayout->addWidget(quitBtn, 3, 4, 1, 1);


        retranslateUi(PictureGet);

        QMetaObject::connectSlotsByName(PictureGet);
    } // setupUi

    void retranslateUi(QWidget *PictureGet)
    {
        PictureGet->setWindowTitle(QApplication::translate("PictureGet", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("PictureGet", "Address:", 0, QApplication::UnicodeUTF8));
        urlEdit->setHtml(QApplication::translate("PictureGet", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">http://captcha.qq.com/getimage?aid=1007901&amp;r=0.835430944347716</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("PictureGet", "Number:", 0, QApplication::UnicodeUTF8));
        numberEdit->setText(QApplication::translate("PictureGet", "5", 0, QApplication::UnicodeUTF8));
        startBtn->setText(QApplication::translate("PictureGet", "Start", 0, QApplication::UnicodeUTF8));
        quitBtn->setText(QApplication::translate("PictureGet", "Quit", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PictureGet: public Ui_PictureGet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_H
