/****************************************************************************
** Meta object code from reading C++ file 'pictureget.h'
**
** Created: Sun Dec 1 17:04:52 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../pictureget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pictureget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PictureGet[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   12,   11,   11, 0x0a,
      48,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PictureGet[] = {
    "PictureGet\0\0reply\0replyFinished(QNetworkReply*)\0"
    "on_startBtn_clicked()\0"
};

void PictureGet::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PictureGet *_t = static_cast<PictureGet *>(_o);
        switch (_id) {
        case 0: _t->replyFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 1: _t->on_startBtn_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PictureGet::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PictureGet::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_PictureGet,
      qt_meta_data_PictureGet, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PictureGet::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PictureGet::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PictureGet::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PictureGet))
        return static_cast<void*>(const_cast< PictureGet*>(this));
    if (!strcmp(_clname, "Ui::PictureGet"))
        return static_cast< Ui::PictureGet*>(const_cast< PictureGet*>(this));
    return QWidget::qt_metacast(_clname);
}

int PictureGet::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
